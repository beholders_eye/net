# Copyright 2021-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=grafana project=loki tag=v${PV} ] \
    flag-o-matic \
    systemd-service [ systemd_files=[ tools/packaging/promtail.service ] ]

SUMMARY="Agent which ships the contents of local logs to a private Loki instance or Grafana Cloud"

LICENCES="AGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# TODO: need to figure out
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.20]
    build+run:
        group/promtail
        user/promtail
"

pkg_setup() {
    filter-ldflags -Wl,-O1 -Wl,--as-needed
}

src_unpack() {
    default

    edo pushd "${WORK}"
    esandbox disable_net
    edo go mod download -x
    esandbox enable_net
    edo popd
}

src_prepare() {
    default

    export GOROOT="/usr/$(exhost --target)/lib/go"
    export GOPATH="${WORKBASE}"/build

    edo mkdir -p "${WORKBASE}"/build/src/github.com/grafana/loki
    edo ln -s "${WORK}" "${WORKBASE}"/build/src/github.com/grafana/loki

    edo ln -s "${TEMP}"/go/pkg "${WORKBASE}"/build

    edo sed \
        -e 's:/tmp:/var/lib/promtail:g' \
        -i clients/cmd/promtail/promtail-journal.yaml
}

src_compile() {
    GO111MODULE="auto" edo go build \
        -o bin/${PN} \
        -tags='promtail_journal_enabled' \
        -ldflags "${LDFLAGS} -X github.com/grafana/loki/pkg/util/build.Version=${PV}" \
        -v \
        "./clients/cmd/${PN}"
}

src_install() {
    dobin bin/${PN}

    insinto /etc/${PN}
    newins clients/cmd/promtail/promtail-journal.yaml config.yml

    install_systemd_files

    keepdir /var/lib/${PN}
    edo chown -R promtail:promtail "${IMAGE}"/var/lib/${PN}
}

