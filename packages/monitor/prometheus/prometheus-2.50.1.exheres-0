# Copyright 2021-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

require github [ tag="v${PV}" ]

SUMMARY="Monitoring system and time series database"
HOMEPAGE+=" https://prometheus.io"

DOWNLOADS+="
    https://github.com/${PN}/${PN}/releases/download/v${PV}/${PN}-web-ui-${PV}.tar.gz
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

# requires dependency on node to run npm build
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.20]
        monitor/promu
    build+run:
        group/prometheus
        user/prometheus
"

src_unpack() {
    default

    export GOMODCACHE="${FETCHEDDIR}/go/pkg/mod"

    esandbox disable_net
    edo pushd "${WORK}"
    edo go mod download -x
    edo popd
    esandbox enable_net

    edo rm -rf "${WORK}"/web/ui/static
    edo mv "${WORKBASE}"/static "${WORK}"/web/ui/
}

src_compile() {
    edo scripts/compress_assets.sh
    edo promu build prometheus
    edo promu build promtool
}

src_test() {
    esandbox allow_net --connect "inet:127.0.0.1@9090"
    esandbox allow_net --connect "inet:127.0.0.1@9"
    esandbox allow_net --connect "inet:127.0.0.1@30000-50000"
    esandbox allow_net --connect "inet6:::1@9"
    esandbox allow_net --connect "inet6:::1@9090"
    default
    esandbox disallow_net --connect "inet6:::1@9090"
    esandbox disallow_net --connect "inet6:::1@9"
    esandbox disallow_net --connect "inet:127.0.0.1@30000-50000"
    esandbox disallow_net --connect "inet:127.0.0.1@9"
    esandbox disallow_net --connect "inet:127.0.0.1@9090"
}

src_install() {
    dobin {prometheus,promtool}

    insinto /etc/default
    doins "${FILES}"/${PN}

    insinto /etc/${PN}
    doins documentation/examples/${PN}.yml
    doins -r console_libraries consoles

    install_systemd_files

    keepdir /var/{lib,log}/${PN}
    edo chown -R prometheus:prometheus "${IMAGE}"/var/{lib,log}/${PN}
}

