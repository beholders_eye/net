# Copyright 2009-2010 Nathan McSween
# Copyright 2014-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require libnetfilter systemd-service

SUMMARY="Manipulate netfilter connection tracking table and run High Availability"
DOWNLOADS="${HOMEPAGE}/files/${PNV}.tar.xz"

LICENCES="GPL-2"
PLATFORMS="~amd64"
MYOPTIONS="systemd"

DEPENDENCIES="
    build+run:
        net-libs/libmnl[>=1.0.3]
        net-libs/libnetfilter_conntrack[>=1.0.9]
        net-libs/libnetfilter_cthelper[>=1.0.0]
        net-libs/libnetfilter_cttimeout[>=1.0.0]
        net-libs/libnetfilter_queue[>=1.0.2]
        net-libs/libnfnetlink[>=1.0.1]
        net-libs/libtirpc
        systemd? ( sys-apps/systemd[>=227] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-cthelper
    --enable-cttimeout
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    systemd
)

src_prepare() {
    # fix run path
    edo sed \
        -e 's:/var/run:/run:g' \
        -i doc/stats/conntrackd.conf

    default
}

src_install() {
    default

    insinto /etc/conntrackd
    doins ./doc/stats/conntrackd.conf

    install_systemd_files
}

