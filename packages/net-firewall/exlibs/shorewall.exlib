# Copyright 2013-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require shorewall-base

export_exlib_phases src_install

if [[ ${PN} == shorewall6 ]]; then
    SUMMARY="A gateway/firewall configuration tool for IPv6"
    DOWNLOADS+=" doc? ( mirror://shorewall/$(ever range 1-2)/${PN/6}-$(ever range 1-3)/${PN/6}-docs-html-${PV}.tar.bz2 )"
else
    SUMMARY="A gateway/firewall configuration tool for IPv4"
    DOWNLOADS+=" doc? ( mirror://shorewall/$(ever range 1-2)/${PN}-$(ever range 1-3)/${PN}-docs-html-${PV}.tar.bz2 )"
fi

MYOPTIONS+=" doc [[ description = [ Install tons of HTML docs, tutorials & HowTos ] ]]"

shorewall_src_install() {
    shorewall-base_src_install

    if [[ ${PN} == shorewall6 ]]; then
        dodoc -r Samples6
    else
        dodoc -r Contrib
        dodoc -r Samples
    fi

    if option doc ; then
        docinto html
        edo pushd "${WORKBASE}"/shorewall-docs-html-${PV}
        dodoc -r *
        edo popd
    fi

    keepdir /usr/share/${PN}/deprecated

    if [[ ${PN} == shorewall ]]; then
        edo rmdir "${IMAGE}"/usr/$(exhost --target)/bin
    fi

    if [[ ${PN} == shorewall6 ]]; then
        edo rmdir "${IMAGE}"/usr/share/${PN/6}/{Shorewall,}
    fi
}

