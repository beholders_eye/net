# Copyright 2022-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix=https://code.videolan.org user=rist tag=v${PV} new_download_scheme=true ] \
    meson

SUMMARY="Reliable Internet Stream Transport (RIST)"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# network sandbox errors
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/cjson
        dev-libs/mbedtls
    test:
        dev-util/cmocka
"

MESON_SOURCE="${WORKBASE}"/${PN}-v${PV}

MESON_SRC_CONFIGURE_PARAMS=(
    -Dallow_insecure_iv_fallback=false
    -Dallow_obj_filter=false
    -Dbuilt_tools=false
    -Dbuiltin_cjson=false
    -Dbuiltin_mbedtls=false
    -Dfallback_builtin=false
    -Dhave_mingw_pthreads=false
    -Dstatic_analyze=false
    -Duse_gnutls=false
    -Duse_mbedtls=true
    -Duse_nettle=false
    -Duse_tun=false
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtest=true -Dtest=false'
)

