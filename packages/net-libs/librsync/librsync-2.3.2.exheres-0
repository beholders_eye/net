# Copyright 2010 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake
require github [ release=v${PV} suffix=tar.gz ]

SUMMARY="Implementation of the rsync remote-delta algorithm"
DESCRIPTION="
librsync is a free software library that implements the rsync remote-delta algorithm. This algorithm
allows efficient remote updates of a file, without requiring the old and new versions to both be
present at the sending end. The library uses a \"streaming\" design similar to that of zlib with the
aim of allowing it to be embedded into many different applications. librsync is currently pre-1.0,
with most important functionality working. librsync is not wire-compatible with rsync 2.x, and is
not likely to be in the future.
"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-doc/doxygen
        virtual/pkg-config
    build+run:
        dev-libs/popt [[ note = [ rdiff executable ] ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -USE_LIBB2:BOOL=OFF
    # Compression is not recommended by upstream, disable it with deps
    -DCMAKE_DISABLE_FIND_PACKAGE_BZip2:BOOL=ON
    -DCMAKE_DISABLE_FIND_PACKAGE_ZLIB:BOOL=ON
    -DENABLE_COMPRESSION:BOOL=OFF
)

