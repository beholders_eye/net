# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=HyperKitty
MY_PNV=${MY_PN}-${PV}

require pypi setup-py [ blacklist=2 import=setuptools work=${MY_PNV} test=pytest ]

SUMMARY="A web interface to access GNU Mailman v3 archives"

HOMEPAGE+=" https://list.org/"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/isort[python_abis:*(-)?]
    build+run:
        dev-python/Django[>=3.2&<4][python_abis:*(-)?]
        dev-python/django-compressor[>=1.3][python_abis:*(-)?]
        dev-python/django-extensions[>=1.3.7][python_abis:*(-)?]
        dev-python/django-gravatar2[>=1.0.6][python_abis:*(-)?]
        dev-python/django-haystack[>=2.8.0][python_abis:*(-)?]
        dev-python/django-mailman3[>=1.3.8][python_abis:*(-)?]
        dev-python/django-q2[>=1.0.0][python_abis:*(-)?]
        dev-python/djangorestframework[>=3.0.0][python_abis:*(-)?]
        dev-python/flufl-lock[>=4.0][python_abis:*(-)?]
        dev-python/mailmanclient[>=3.3.3][python_abis:*(-)?]
        dev-python/mistune[>=2.0.0&<3.0][python_abis:*(-)?]
        dev-python/networkx[>=2.0][python_abis:*(-)?]
        dev-python/python-dateutil[>=2.0][python_abis:*(-)?]
        dev-python/pytz[>=2012][python_abis:*(-)?]
        dev-python/robot-detection[>=0.3][python_abis:*(-)?]
    test:
        dev-python/Whoosh[>=2.5.7][python_abis:*(-)?]
        dev-python/beautifulsoup4[>=4.3.2][python_abis:*(-)?]
        dev-python/elasticsearch[python_abis:*(-)?]
        dev-python/django-debug-toolbar[python_abis:*(-)?]
        dev-python/lxml[python_abis:*(-)?]
        dev-python/pytest-django[python_abis:*(-)?]
"

PYTEST_PARAMS=(
    # TODO: Fails with "AttributeError: 'str' object has no attribute 'status'"
    -k "not test_parse_error"
)

