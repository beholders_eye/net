# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ blacklist=2 import=setuptools test=nose ]

SUMMARY="The GNU mailing list manager"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/aiosmtpd[>=1.4.3][python_abis:*(-)?]
        dev-python/alembic[>1.7.0][python_abis:*(-)?]
        dev-python/atpublic[python_abis:*(-)?]
        dev-python/authheaders[>=0.15.2][python_abis:*(-)?]
        dev-python/authres[>=1.0.1][python_abis:*(-)?]
        dev-python/click[>=8.0][python_abis:*(-)?]
        dev-python/dnspython[>=1.14.0][python_abis:*(-)?]
        dev-python/falcon[>=3.0.0][python_abis:*(-)?]
        dev-python/flufl-bounce[>=4.0][python_abis:*(-)?]
        dev-python/flufl-i18n[>=3.2][python_abis:*(-)?]
        dev-python/flufl-lock[>=5.1][python_abis:*(-)?]
        dev-python/lazr-config[python_abis:*(-)?]
        dev-python/passlib[python_abis:*(-)?]
        dev-python/python-dateutil[>=2.0][python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
        dev-python/SQLAlchemy[>=1.4.0][python_abis:*(-)?]
        dev-python/zopecomponent[python_abis:*(-)?]
        dev-python/zopeconfiguration[python_abis:*(-)?]
        dev-python/zopeevent[python_abis:*(-)?]
        dev-python/zopeinterface[>=5.0][python_abis:*(-)?]
        user/mailman
        www-servers/gunicorn:0[python_abis:*(-)?]
    run:
        virtual/mta
"

# Wants to access a SMTP server on port 25
# Also https://gitlab.com/mailman/mailman/-/issues/400
RESTRICT="test"

test_one_multibuild() {
    esandbox allow_net --connect "inet:127.0.0.1@8001"
    esandbox allow_net --connect "inet:127.0.0.1@8024"
    esandbox allow_net --connect "inet:127.0.0.1@9001"
    setup-py_test_one_multibuild
    esandbox disallow_net --connect "inet:127.0.0.1@9001"
    esandbox disallow_net --connect "inet:127.0.0.1@8024"
    esandbox disallow_net --connect "inet:127.0.0.1@8001"
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    # Work around an issue with pdm-backend and flufl-{i18n/lock}
    # https://gitlab.com/mailman/mailman/-/issues/1085
    edo sed \
        -e "s/flufl.i18n/flufl-i18n/" \
        -e "s/flufl.lock/flufl-lock/" \
        -i "${IMAGE}"/$(python_get_sitedir)/${PNV}-py$(python_get_abi).egg-info/requires.txt
}

