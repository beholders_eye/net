# Copyright 2017-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

GSA_PV=23.0.0

require github [ user=greenbone project=gsad tag=v${PV} ] \
    cmake \
    systemd-service

SUMMARY="Greenbone Security Assistant"
HOMEPAGE+=" https://www.openvas.org"
DOWNLOADS+=" https://github.com/greenbone/gsa/releases/download/v${GSA_PV}/gsa-dist-${GSA_PV}.tar.gz"

LICENCES="AGPL-3 BSD-3 MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# requires unpackaged cgreen, last checked: 22.9.1
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        group/gvm
        user/gvm
        app-arch/brotli
        dev-libs/glib:2[>=2.42]
        dev-libs/gnutls[>=3.2.15]
        dev-libs/libgcrypt
        dev-libs/libxml2:2.0
        net-analyzer/gvm-libs[>=22.6]
        net-libs/libmicrohttpd[>=0.9.0][ssl]
        sys-libs/zlib[>=1.2]
    run:
        net-analyzer/gvmd
    test:
        dev-util/cppcheck
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_TESTING:BOOL=FALSE
    -DCLANG_FORMAT:BOOL=FALSE
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DDATADIR:PATH=/usr/share
    -DDOXYGEN_EXECUTABLE:BOOL=FALSE
    -DLIBDIR:PATH=/usr/$(exhost --target)/lib
    -DSBINDIR:PATH=/usr/$(exhost --target)/bin
    -DXMLMANTOHTML_EXECUTABLE:BOOL=FALSE
    -DXMLTOMAN_EXECUTABLE:BOOL=FALSE
)

src_prepare() {
    cmake_src_prepare

    # for now we keep installing our own
    edo sed \
        -e '/add_subdirectory (config)/d' \
        -i CMakeLists.txt
}

src_install() {
    cmake_src_install

    insinto /usr/share/gvm/gsad/web
    doins -r ${WORKBASE}/{build,img,locales,static,index.html,robots.txt}

    keepdir /var/log/gvm
    edo chown gvm:gvm "${IMAGE}"/var/log/gvm

    install_systemd_files

    insinto /etc/conf.d
    hereins gsad.conf << EOF
GSAD_LISTEN=--listen=127.0.0.1
GSAD_PORT=--port=9392
GSAD_HTTP=--http-only
#GSAD_REDIRECT=--no-redirect
#GSAD_SSL_CERT=--ssl-certificate=/etc/ssl/openvas.crt
#GSAD_SSL_KEY=--ssl-private-key=/etc/ssl/openvas.key
EOF

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/gsad 0755 gvm gvm
EOF
}

