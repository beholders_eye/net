# Copyright 2013 Kevin Decherf <kevin@kdecherf.com>
# Copyright 2021 Arnaud Lefebvre <a.lefebvre@outlook.fr>
# Distributed under the terms of the GNU General Public License v2

require python [ blacklist=2 multibuild=false ]
require cmake
require systemd-service [ systemd_files=[ ] systemd_tmpfiles=[ "${CMAKE_SOURCE}/systemd/${PN}.conf" ] ]

SUMMARY="Ceph is a distributed object store and file system"
HOMEPAGE="https://ceph.com"
DOWNLOADS="https://download.ceph.com/tarballs/${PNV}.tar.gz"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    amqp   [[ description = [ Rados Gateway's pubsub support for AMQP endpoint ] ]]
    client   [[ description = [ Tools and libraries necessary for connecting to a Ceph cluster ] ]]
    debug    [[ description = [ Additional tools and tests for debugging Ceph clusters ] ]]
    gateway  [[ description = [ The RADOS Gateway, which provides OpenStack Swift and Amazon S3-compatible API access ] ]]
    fuse     [[ description = [ FUSE client for CephFS ] ]]
    kafka    [[ description = [ Rados Gateway's pubsub support for Kafka endpoint ] ]]
    man-pages
    profiler [[ description = [ Performance profiling with Google perf tools ] ]]
    client? ( ( providers: eudev systemd ) [[ number-selected = exactly-one ]] )
    ( providers: openssl libressl ) [[ number-selected = exactly-one ]]
"

RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/yasm
        dev-python/Cython[python_abis:*(-)?]
        dev-python/setuptools[python_abis:*(-)?]
        dev-python/virtualenv[python_abis:*(-)?]
        dev-util/cunit
        man-pages? ( dev-python/Sphinx[python_abis:*(-)?] )
    build+run:
        user/${PN}
        group/${PN}
        amqp? ( net-libs/librabbitmq )
        app-arch/lz4
        app-arch/snappy
        app-misc/mailcap
        app-misc/oath-toolkit
        dev-db/leveldb[>=1.20.0]
        dev-libs/boost[>=1.81.0][python][python_abis:*(-)?]
        dev-libs/jemalloc
        dev-libs/libaio
        dev-libs/libatomic_ops
        dev-libs/nspr
        dev-libs/nss
        net-misc/curl
        sys-apps/keyutils
        sys-fs/e2fsprogs
        sys-fs/xfsprogs
        client? (
            app-misc/oath-toolkit
            dev-libs/expat
            net-libs/libnl:3.0 [[ note = [ genl ] ]]
            sys-apps/util-linux [[ note = [ libblkid ] ]]
            sys-libs/libcap-ng
            providers:eudev? ( sys-apps/eudev )
            providers:systemd? ( sys-apps/systemd )
        ) [[ note = [ rbd {map,unmapped,showmap} support ] ]]
        fuse? ( sys-fs/fuse:0 )
        gateway? (
            dev-libs/expat
        ) [[ note = [ uses bundled libs3 ] ]]
        kafka? ( dev-libs/librdkafka )
        profiler? ( dev-util/gperftools )
        providers:openssl? ( dev-libs/openssl:= )
        providers:libressl? ( dev-libs/libressl:= )
    run:
        dev-python/CherryPy[python_abis:*(-)?]
        dev-python/pecan[python_abis:*(-)?]
        dev-python/PrettyTable[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        dev-python/Werkzeug[python_abis:*(-)?]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/hostname-fix.patch
    "${FILES}"/0002-fix-gcc-spdk.patch
    "${FILES}"/mgr-python-version.patch
    "${FILES}"/${PN}-compression-snappy-use-uint32_t-to-be-compatible-wit.patch
    "${FILES}"/0001-include-buffer-include-memory.patch
    "${FILES}"/0001-common-fix-FTBFS-due-to-dout-need_dynamic-on-GCC-12.patch
    "${FILES}"/0001-rgw-rgw_string.h-add-missing-includes-for-alpine-and.patch
    "${FILES}"/0001-error-has-no-member-named-to_string-boost-1.81.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_PREFIX:PATH=/usr/$(exhost --target)
    -DCMAKE_INSTALL_FULL_SBINDIR:PATH=/usr/$(exhost --target)/bin
    -DCMAKE_INSTALL_SBINDIR=bin
    -DCMAKE_C_COMPILER:PATH=/usr/host/bin/$(exhost --tool-prefix)cc
    -DCMAKE_CC:PATH=/usr/host/bin/$(exhost --tool-prefix)cc
    -DCMAKE_AR:PATH=/usr/host/bin/$(exhost --tool-prefix)ar
    -DCMAKE_LD:PATH=/usr/host/bin/$(exhost --tool-prefix)ld
    -DCROSS=$(exhost --tool-prefix)
    # Disable unused stuff. Add matching option if you want to enable it.
    -DWITH_ASAN=OFF
    -DWITH_ASAN_LEAK=OFF
    -DWITH_OPENLDAP=OFF
    -DWITH_RDMA=OFF
    -DWITH_LTTNG=OFF
    -DWITH_RADOSGW_FCGI_FRONTEND=OFF
    -DWITH_SELINUX=OFF
    -DWITH_TSAN=OFF
    -DWITH_UBSAN=OFF
    -DWITH_BABELTRACE=OFF
    -DHAVE_BABELTRACE=OFF
    # Disable dashboard for mgr. It downloads boost 1.67 and runs some pip install stuff.
    # I don't want it for now.
    -DWITH_MGR_DASHBOARD_FRONTEND=OFF
    # Enable shared system boost. Add option if you want to disable
    -DWITH_SYSTEM_BOOST=ON
    -DWITH_XFS=ON
    # There should be better performances with jemalloc
    -DALLOCATOR="jemalloc"
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'amqp RADOSGW_AMQP_ENDPOINT'
    # The RADOS Block Device API - needed by qemu[ceph], among others
    'client RBD'
    # The RADOS Block Device API - needed by qemu[ceph], among others
    'client KRBD'
    # CephFS - the POSIX-compatible Ceph cluster filesystem
    'client CEPHFS'
    'client LIBCEPHFS'
    'fuse FUSE'
    'gateway RADOSGW'
    'kafka RADOSGW_KAFKA_ENDPOINT'
    'man-pages MANPAGE'
    'providers:systemd SYSTEMD'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DWITH_TESTS:BOOL=TRUE -DWITH_TESTS:BOOL=FALSE'
)

src_prepare() {
    edo sed -i -e "/set(MGR_PYTHON_VERSION/s/2.7/$(python_get_abi)/" ${WORKBASE}/${PNV}/CMakeLists.txt
    edo sed -i -e 's/DESTINATION sbin/DESTINATION bin/' ${WORKBASE}/${PNV}/src/CMakeLists.txt
    edo sed -i 's/CMAKE_INSTALL_LIBEXECDIR/CMAKE_INSTALL_LIBDIR/' ${WORKBASE}/${PNV}/systemd/CMakeLists.txt
    edo sed -i 's;/usr/lib/ceph/ceph-osd-prestart.sh;/usr/host/libexec/ceph/ceph-osd-prestart.sh;' \
        ${WORKBASE}/${PNV}/systemd/ceph-osd@.service.in

    edo mv ${WORKBASE}/${PNV}/systemd/${PN}.tmpfiles.d ${WORKBASE}/${PNV}/systemd/${PN}.conf

    # ceph embeds dependencies with hardcoded calls to unprefixed tools
    # This is mostly copied from rust-build.exlib
    local dir="${WORKBASE}"/symlinked-build-tools
    local tool
    edo mkdir -p "${dir}"
    for tool in ar cc g++ gcc ld; do
        edo ln -s /usr/host/bin/$(exhost --tool-prefix)${tool} "${dir}"/${tool}
    done

    export PATH="${dir}:${PATH}"

    CC="/usr/host/bin/$(exhost --tool-prefix)cc" cmake_src_prepare
}

src_configure() {
    LDFLAGS="${LDFLAGS} -lpthread" CC="/usr/host/bin/$(exhost --tool-prefix)cc" cmake_src_configure
}

src_install() {
    default

    keepdir /etc/${PN} /var/log/${PN} /var/lib/${PN}/tmp
    nonfatal edo rmdir "${IMAGE}"/usr/share/ceph/mgr/osd_perf_query
    nonfatal edo rmdir "${IMAGE}"/usr/share/ceph/mgr/hello
    install_systemd_files
}

