# Copyright 2023-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SCM_Simple_Web_Server_REPOSITORY="https://gitlab.com/eidheim/Simple-Web-Server.git"
SCM_Simple_Web_Server_REVISION="27b41f5ee154cca0fce4fe2955dd886d04e3a4ed"
SCM_TPCircularBuffer_REPOSITORY="https://github.com/michaeltyson/TPCircularBuffer.git"
SCM_TPCircularBuffer_REVISION="8833b3a73fab6530cc51e2063a85cced01714cfb"
SCM_ViGEmClient_REPOSITORY="https://github.com/LizardByte/Virtual-Gamepad-Emulation-Client.git"
SCM_ViGEmClient_REVISION="8d71f6740ffff4671cdadbca255ce528e3cd3fef"
SCM_build_deps_REPOSITORY="https://github.com/LizardByte/build-deps.git"
SCM_build_deps_REVISION="efd3a380113e8ae98ae68cc1d73fd7c4b54b03c6"
SCM_build_deps_BRANCH="dist"
SCM_googletest_REPOSITORY="https://github.com/google/googletest.git"
SCM_googletest_REVISION="f8d7d77c06936315286eb55f8de22cd23c188571"
SCM_googletest_BRANCH="main"
SCM_moonlight_common_c_REPOSITORY="https://github.com/moonlight-stream/moonlight-common-c.git"
SCM_moonlight_common_c_REVISION="cbd0ec1b25edfb8ee8645fffa49ff95b6e04c70e"
SCM_moonlight_common_c_enet_REPOSITORY="https://github.com/cgutman/enet.git"
SCM_moonlight_common_c_enet_REVISION="04e27590670a87a7cd40f5a05cda97467e4e25a3"
SCM_nanors_REPOSITORY="https://github.com/sleepybishop/nanors.git"
SCM_nanors_REVISION="e9e242e98e27037830490b2a752895ca68f75f8b"
SCM_nv_codec_headers_REPOSITORY="https://github.com/FFmpeg/nv-codec-headers.git"
SCM_nv_codec_headers_REVISION="22441b505d9d9afc1e3002290820909846c24bdc"
SCM_nv_codec_headers_BRANCH="sdk/12.0"
SCM_nvapi_open_source_sdk_REPOSITORY="https://github.com/LizardByte/nvapi-open-source-sdk.git"
SCM_nvapi_open_source_sdk_REVISION="c0f5f7b64d2ef13b1155f078a2eec156611f2415"
SCM_nvapi_open_source_sdk_BRANCH="sdk"
SCM_tray_REPOSITORY="https://github.com/LizardByte/tray.git"
SCM_tray_REVISION="4d8b798cafdd11285af9409c16b5f792968e0045"
SCM_wayland_protocols_REPOSITORY="https://gitlab.freedesktop.org/wayland/wayland-protocols"
SCM_wayland_protocols_REVISION="46f201bd7b328ab5ac531231c030ca5c4090b1da"
SCM_wayland_protocols_BRANCH="main"
SCM_wlr_protocols_REPOSITORY="https://gitlab.freedesktop.org/wlroots/wlr-protocols"
SCM_wlr_protocols_REVISION="4264185db3b7e961e7f157e1cc4fd0ab75137568"
SCM_org_flatpak_Builder_BaseApp_REPOSITORY="https://github.com/flathub/org.flatpak.Builder.BaseApp.git"
SCM_org_flatpak_Builder_BaseApp_REVISION="6e295e630740ae8ef82c6291724e709b36477042"
SCM_org_flatpak_Builder_BaseApp_BRANCH="branch/23.08"
SCM_shared_modules_REPOSITORY="https://github.com/flathub/shared-modules.git"
SCM_shared_modules_REVISION="d0229951ac23967c4f5697bd7b5c1bd7e641b8c3"

SCM_SECONDARY_REPOSITORIES="
    Simple_Web_Server
    TPCircularBuffer
    ViGEmClient
    build_deps
    googletest
    moonlight_common_c
    moonlight_common_c_enet
    nanors
    nv_codec_headers
    nvapi_open_source_sdk
    tray
    wayland_protocols
    wlr_protocols
    org_flatpak_Builder_BaseApp
    shared_modules
"
SCM_moonlight_common_c_SECONDARY_REPOSITORIES="
    moonlight_common_c_enet
"

SCM_EXTERNAL_REFS="
    third-party/Simple-Web-Server:Simple_Web_Server
    third-party/TPCircularBuffer:TPCircularBuffer
    third-party/ViGEmClient:ViGEmClient
    third-party/build-deps:build_deps
    third-party/googletest:googletest
    third-party/moonlight-common-c:moonlight_common_c
    third-party/nanors:nanors
    third-party/nv-codec-headers:nv_codec_headers
    third-party/nvapi-open-source-sdk:nvapi_open_source_sdk
    third-party/tray:tray
    third-party/wayland-protocols:wayland_protocols
    third-party/wlr-protocols:wlr_protocols
    packaging/linux/flatpak/deps/org.flatpak.Builder.BaseApp:org_flatpak_Builder_BaseApp
    packaging/linux/flatpak/deps/shared-modules:shared_modules
"
SCM_moonlight_common_c_EXTERNAL_REFS="
    enet:moonlight_common_c_enet
"

require github [ user=LizardByte pn=Sunshine tag=v${PV} force_git_clone=true ] \
    cmake \
    systemd-service

SUMMARY="Self-hosted game stream host for Moonlight"
HOMEPAGE+=" https://app.lizardbyte.dev/Sunshine/"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    wayland
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/node
        sys-apps/systemd
        virtual/pkg-config
        wayland? ( sys-libs/wayland-protocols )
    build+run:
        dev-libs/boost[>=1.53.0]
        media-libs/opus
        media-sound/pulseaudio
        net-libs/miniupnpc
        net-misc/curl
        sys-apps/numactl
        sys-libs/libcap
        x11-dri/libdrm
        x11-libs/libevdev
        x11-libs/libva
        x11-libs/libvdpau
        x11-libs/libX11
        x11-libs/libXtst
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        wayland? ( sys-libs/wayland )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/7e26d2fd3064c6adf3e7a52a275046dd0cf194be.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_FUZZING:BOOL=FALSE
    -DBUILD_TESTS:BOOL=FALSE
    -DBUILD_WERROR:BOOL=FALSE
    -DCMAKE_INSTALL_BINDIR:PATH=/usr/$(exhost --target)/bin
    -DCMAKE_INSTALL_LIBDIR:PATH=/usr/$(exhost --target)/lib
    -DCMAKE_INSTALL_PREFIX:PATH=/usr
    -DENET_NO_INSTALL:BOOL=TRUE
    -DSUNSHINE_ASSETS_DIR:PATH=share/sunshine
    -DSUNSHINE_BUILD_APPIMAGE:BOOL=FALSE
    -DSUNSHINE_BUILD_HOMEBREW:BOOL=FALSE
    -DSUNSHINE_BUILD_FLATPAK:BOOL=FALSE
    -DSUNSHINE_CONFIGURE_FLATPAK_MAN:BOOL=FALSE
    -DSUNSHINE_CONFIGURE_ONLY:BOOL=FALSE
    -DSUNSHINE_CONFIGURE_PKGBUILD:BOOL=FALSE
    -DSUNSHINE_ENABLE_CUDA:BOOL=FALSE
    -DSUNSHINE_ENABLE_DRM:BOOL=TRUE
    -DSUNSHINE_ENABLE_TRAY:BOOL=FALSE
    -DSUNSHINE_ENABLE_VAAPI:BOOL=TRUE
    -DSUNSHINE_ENABLE_X11:BOOL=TRUE
    -DSUNSHINE_EXECUTABLE_PATH:PATH=/usr/$(exhost --target)/bin/sunshine
    -DSUNSHINE_REQUIRE_TRAY:BOOL=FALSE
    -DSUNSHINE_SYSTEM_WAYLAND_PROTOCOLS:BOOL=TRUE
    -DSYSTEMD_USER_UNIT_INSTALL_DIR:PATH=${SYSTEMDUSERUNITDIR}
    -DTESTS_ENABLE_PYTHON_TESTS:BOOL=FALSE
    -DTESTS_SOFTWARE_ENCODER_UNAVAILABLE:STRING=skip
    -DUSE_OPENSSL:BOOL=TRUE
    -DUSE_STANDALONE_ASIO:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'wayland SUNSHINE_ENABLE_WAYLAND'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

src_unpack() {
    cmake_src_unpack

    edo pushd "${CMAKE_SOURCE}"
    esandbox disable_net
    edo npm install
    esandbox enable_net
    edo popd
}

src_prepare() {
    cmake_src_prepare

    # remove dependency on unpackaged libmfx
    edo sed \
        -e '/mfx/d' \
        -i cmake/dependencies/common.cmake
}

pkg_postinst() {
    # required for screencasting with KMS
    edo setcap cap_sys_admin+p /usr/$(exhost --target)/bin/${PNV}
}

