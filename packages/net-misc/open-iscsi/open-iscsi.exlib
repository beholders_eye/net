# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=${PN} tag=${PV} ] meson systemd-service udev-rules

export_exlib_phases src_install

SUMMARY="An IP-based storage networking standard for linking data storage facilities"
DESCRIPTION="
The Open-iSCSI project is a high performance, transport independent, multi-platform
implementation of RFC3720, the RFC that specifies the Internet Small Computer
Systems Interface (iSCSI).
Linux-iSCSI was merged with Open-iSCSI in 2005 and, thus, is obsolete.
"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    systemd
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# requires /run/lock/iscsi to be present
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        net-misc/open-isns
        sys-apps/kmod
        sys-apps/util-linux
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        systemd? ( sys-apps/systemd )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddbroot=/var/lib/iscsi
    -Dhomedir=/etc/iscsi
    -Diqn_prefix=iqn.2016-04.com.open-iscsi
    -Discsi_sbindir=/usr/$(exhost --target)/bin
    -Disns=enabled
    -Dlockdir=/run/lock/iscsi
    -Drulesdir=${UDEVRULESDIR}
    -Dsystemddir=${SYSTEMDSYSTEMUNITDIR%/system}
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    '!systemd no_systemd'
)

open-iscsi_src_install() {
    meson_src_install

    keepdir /var/lib/iscsi
}

